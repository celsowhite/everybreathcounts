<?php
global $qode_options_proya;
$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments'])) {
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];
}
$blog_share_like_layout = 'in_post_info';
if (isset($qode_options_proya['blog_share_like_layout'])) {
    $blog_share_like_layout = $qode_options_proya['blog_share_like_layout'];
}
$enable_social_share = 'no';
if(isset($qode_options_proya['enable_social_share'])){
    $enable_social_share = $qode_options_proya['enable_social_share'];
}
$blog_author_info="no";
if (isset($qode_options_proya['blog_author_info'])) {
	$blog_author_info = $qode_options_proya['blog_author_info'];
}
$qode_like = "on";
if (isset($qode_options_proya['qode_like'])) {
    $qode_like = $qode_options_proya['qode_like'];
}

$gallery_post_layout = qode_check_gallery_post_layout(get_the_ID());

$params = array(
    'blog_share_like_layout' => $blog_share_like_layout,
    'enable_social_share' => $enable_social_share,
    'qode_like' => $qode_like
);

$_post_format = get_post_format();
?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="post_content_holder">
				<?php if(get_post_meta(get_the_ID(), "qode_hide-featured-image", true) != "yes") {
					if ( has_post_thumbnail() ) { ?>
						<div class="post_image">
              <?php the_post_thumbnail('full'); ?>
						</div>
				<?php } } ?>
				<div class="post_text">
					<div class="post_text_inner">


            <!-- .partner-logos -->
            <?php
              if (get_field('partners_featured_logos')):
                $logos = get_field('partners_featured_logos');
                $size = 'medium';
            ?>
              <div class="partner-logos">
                <?php
                  if( $logos ):
                    foreach( $logos as $image ): ?>
                      <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                    <?php
                    endforeach;
                  endif; ?>
              </div>
            <?php
              endif
            ?>
            <!-- /.partner-logos -->


            <?php
              if (have_rows('partners_layout')):
                while (have_rows('partners_layout')): the_row();
                  $partnersLayout = get_field('partners_layout');
                  if( get_row_layout() == 'partner_section' ):
            ?>
                  <?php
                    if (get_sub_field('partners_heading')): ?>
                  	<br>
                      <h3 style="color: #000;"><?= the_sub_field('partners_heading'); ?></h3>
                  <?php
                    endif ?>

                    <?php
                    if (get_sub_field('partners_list')):
                      while (have_rows('partners_list')): the_row();
                        $size = 'medium';
                        $logo = get_sub_field('partner_logo');
                        $content = get_sub_field('partner_content');
                    ?>


                      <div class="media-row media-row--left">
                        <div>
                          <?php
                            if( $logo ):
                              foreach( $logo as $image ): ?>
                                <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                              <?php
                              endforeach;
                            endif; ?>
                        </div>
                        <div>
                          <?php echo $content; ?>
                        </div>
                      </div>
                    <?php
                      endwhile;
                    endif; ?>
            <?php
                  endif;
                endwhile;
              endif;
            ?>

						<?php
              // the_content();
            ?>
					</div>
				</div>
			</div>


	<?php if( has_tag()) { ?>
		<div class="single_tags clearfix">
            <div class="tags_text">
				<h5><?php _e('Tags:','qode'); ?></h5>
				<?php
				if ((isset($qode_options_proya['tags_border_style']) && $qode_options_proya['tags_border_style'] !== '') || (isset($qode_options_proya['tags_background_color']) && $qode_options_proya['tags_background_color'] !== '')){
					the_tags('', ' ', '');
				}
				else{
					the_tags('', ', ', '');
				}
				?>
			</div>
		</div>
	<?php } ?>
    <?php qode_get_template_part('templates/blog-parts/blog','share-like-below-text',$params); ?>
	<?php
		$args_pages = array(
			'before'           => '<p class="single_links_pages">',
			'after'            => '</p>',
			'link_before'      => '<span>',
			'link_after'       => '</span>',
			'pagelink'         => '%'
		);

		wp_link_pages($args_pages);
	?>
<?php if($blog_author_info == "yes") { ?>
	<div class="author_description">
		<div class="author_description_inner">
			<div class="image">
				<?php echo get_avatar(get_the_author_meta( 'ID' ), 75); ?>
			</div>
			<div class="author_text_holder">
				<h5 class="author_name vcard author">
				<span class="fu">
				<?php
					if(get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "") {
						echo get_the_author_meta('first_name') . " " . get_the_author_meta('last_name');
					} else {
						echo get_the_author_meta('display_name');
					}
				?>
			    </span>
				</h5>
				<span class="author_email"><?php echo get_the_author_meta('email'); ?></span>
				<?php if(get_the_author_meta('description') != "") { ?>
					<div class="author_text">
						<p><?php echo get_the_author_meta('description') ?></p>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>
</article>
