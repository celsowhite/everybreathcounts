<!-- Header -->
<?php
  $header_img = !empty(get_field('ebc_hero_image')) ? get_field('ebc_hero_image') : get_stylesheet_directory_uri().'/img/ebc_family.jpg';
?>
<div class="main_image" style="background-image:url('<?= $header_img ?>')">
  <div class="main_image_content">
    <?php get_template_part('title'); ?>
  </div>
</div>
<!-- /Header -->
