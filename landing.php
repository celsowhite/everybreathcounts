<?php 
/*
Template Name: EBC Landing
*/ 
?>
<?php 
global $wp_query;
$id = $wp_query->get_queried_object_id();
$sidebar = get_post_meta($id, "qode_show-sidebar", true);  

$enable_page_comments = false;
if(get_post_meta($id, "qode_enable-page-comments", true) == 'yes') {
	$enable_page_comments = true;
}

if(get_post_meta($id, "qode_page_background_color", true) != ""){
	$background_color = get_post_meta($id, "qode_page_background_color", true);
}else{
	$background_color = "";
}

$content_style_spacing = "";
if(get_post_meta($id, "qode_margin_after_title", true) != ""){
	if(get_post_meta($id, "qode_margin_after_title_mobile", true) == 'yes'){
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px !important";
	}else{
		$content_style_spacing = "padding-top:".esc_attr(get_post_meta($id, "qode_margin_after_title", true))."px";
	}
}

if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }

?>
	<?php get_header(); ?>
		<?php if(get_post_meta($id, "qode_page_scroll_amount_for_sticky", true)) { ?>
			<script>
			var page_scroll_amount_for_sticky = <?php echo get_post_meta($id, "qode_page_scroll_amount_for_sticky", true); ?>;
			</script>
		<?php } ?>
			<?php get_template_part( 'title' ); ?>
		<?php
		$revslider = get_post_meta($id, "qode_revolution-slider", true);
		if (!empty($revslider)){ ?>
			<div class="q_slider"><div class="q_slider_inner">
			<?php echo do_shortcode($revslider); ?>
			</div></div>
		<?php
		}
		?>
	<div class="full_width"<?php if($background_color != "") { echo " style='background-color:". $background_color ."'";} ?>>
		
		<div class="full_width_inner" <?php qode_inline_style($content_style_spacing); ?>>

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="main_image" style="background-image:url('<?php echo get_stylesheet_directory_uri() . '/img/ebc_family.jpg' ?>')">

					<div class="main_image_content">
						<img class="logo animated fadeIn" src="<?php echo get_stylesheet_directory_uri() . '/img/logowhite.png' ?>" />
						<h1 class="text_white animated fadeIn">Every Breath Counts</h1>
						<p class="text_white animated fadeIn">Pneumonia is the top killer in children under 5.</p>
					</div>

					<div class="down_arrow animated fadeInUp">

						<a class="smooth_scroll" href="#stat_panel">
							
							<i class="fa fa-angle-down"></i>

						</a>

					</div>

					<img class="pattern_overlay_top animated fadeIn"  src="<?php echo get_stylesheet_directory_uri() . '/img/patternoverlay_top_left.png' ?>" />

					<img class="pattern_overlay_bottom animated fadeIn"  src="<?php echo get_stylesheet_directory_uri() . '/img/patternoverlay_bottom_right.png' ?>" />

				</div>
				
				<!-- Stats -->

				<section id="stat_panel" class="ebc_panel background_beige">

					<div class="ebc_row">

						<div class="column_1_3 center">
							<div class="stat_container">
								<div class="stat_graphic">
									<img src="<?php the_field('stat_1_image'); ?>" />
								</div>
								<p class="stat_text">
									<?php the_field('stat_1_text'); ?> 
								</p>
							</div>
						</div>

						<div class="column_1_3 center">
							<div class="stat_container">
								<div class="stat_graphic">
									<img src="<?php the_field('stat_2_image'); ?>" />
								</div>
								<p class="stat_text">
									<?php the_field('stat_2_text'); ?>
								</p>
							</div>
						</div>

						<div class="column_1_3 center">
							<div class="stat_container">
								<div class="stat_graphic">
									<img src="<?php the_field('stat_3_image'); ?>" />
								</div>
								<p class="stat_text">
									<?php the_field('stat_3_text'); ?> 
								</p>
							</div>
						</div>

					</div>

				</section>

				<!-- Campaign Information -->

				<section class="ebc_panel background_white">

					<div class="ebc_row">

						<div class="column_1">

							<div class="split_section">

								<div class="psa_video">

									<img src="<?php the_field('homepage_video_image'); ?>" />

									<a href="<?php the_field('homepage_video_link'); ?>" class="popup-youtube"><i class="fa fa-play-circle"></i></a>

								</div>

								<div class="campaign_info background_beige">

									<?php the_content(); ?>

								</div>

							</div>

							<?php if( have_rows('ebc_slideshow') ): ?>
								
								<div id="ebc_slider" class="flexslider">
								  
								  <ul class="slides">
									
									<?php while( have_rows('ebc_slider') ): the_row(); ?>

									    <li>
									      <img src="<?php the_sub_field('ebc_slide_image'); ?>" />
									      <p class="flex-caption"><?php the_sub_field('ebc_slide_caption'); ?></p>
									    </li>

									<?php endwhile; ?>
								  
								  </ul>

								</div>

							<?php endif; ?>

						</div>

					</div>

				</section>

				<!-- Image -->

				<section id="third_panel" class="ebc_image_panel" style="background-image:url('<?php echo get_stylesheet_directory_uri() . '/img/footer_image.jpg' ?>');">

					<img class="pattern_overlay_top"  src="<?php echo get_stylesheet_directory_uri() . '/img/patternoverlay_top_left.png' ?>" />

					<img class="pattern_overlay_bottom"  src="<?php echo get_stylesheet_directory_uri() . '/img/patternoverlay_bottom_right.png' ?>" />

				</section>

				<!-- Call to actions -->

				<section class="ebc_panel background_beige center">

					<div class="ebc_row">

						<div class="column_1">

							<div class="footer_headline">

								<h2>We can make pneumonia history.</br>For every child. Everywhere.</h2>

							</div>

							<ul class="hashtags">

								<li>#endpneumonia</li>
								<li>#everybreathcounts</li>

							</ul>

							<div id="mc_embed_signup">
								<form action="//speakupafrica.us12.list-manage.com/subscribe/post?u=3e3faf44050a1ab06c80387a7&amp;id=22a6f2dcc9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								    <div id="mc_embed_signup_scroll">
										<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="enter your email for updates" required>
									    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
									    <div style="position: absolute; left: -5000px;"><input type="text" name="b_bfaeb9f9cfe94bf88ebe82105_58d7ecad1c" tabindex="-1" value=""></div>
									    <input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button">
								    </div>
								</form>
							</div>

							<ul class="social_links">
								<li><a href="mailto:info@everybreathcounts.info" target="_blank">
									<i class="fa fa-envelope"></i>
								</a></li>
								<li><a href="https://twitter.com/endpneumonia" target="_blank">
									<i class="fa fa-twitter"></i>
								</a></li>
								<li><a href="https://www.facebook.com/endpneumonia?_rdr=p" target="_blank">
									<i class="fa fa-facebook"></i>
								</a></li>
								<li><a href="https://www.instagram.com/everybreath_counts/" target="_blank">
									<i class="fa fa-instagram"></i>
								</a></li>
							</ul>

						</div>

					</div>

				</section>
			
			<?php endwhile; ?>
			<?php endif; ?>

		</div>

	</div>	
	<?php get_footer(); ?>