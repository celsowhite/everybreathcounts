<?php

/*===================================
Enqueue Styles & Scripts
===================================*/

function wp_load_main_script() {

	/*=== Load Custom Scripts First so they initialize first over other scripts ===*/

	wp_enqueue_script('_s-scripts', get_stylesheet_directory_uri() . '/js/scripts.js', '', '', true);

	/*=== Smooth Scroll ===*/

	wp_enqueue_script('smooth-scroll', get_stylesheet_directory_uri() . '/js/jquery.smooth-scroll.min.js', '', '', true);

}

add_action('wp_enqueue_scripts','wp_load_main_script',1);

function wp_load_files() {

	/*=== FitVids ===*/

	wp_enqueue_script('_s-fitvids', get_stylesheet_directory_uri() . '/js/fitvids/fitvids.min.js', '', '', true);

	/*=== Magnific Popup JS ===*/

	wp_enqueue_script( '_s-magnific', get_stylesheet_directory_uri() . '/js/magnific/jquery.magnific-popup.min.js', '','', true);

	/*=== Magnific Popup CSS ===*/

	wp_enqueue_style('magnific', get_stylesheet_directory_uri() . '/css/magnific/magnific-popup.min.css');

	/*=== Animate CSS ===*/

	wp_enqueue_style('animate_css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css');

	/*=== Compass Compiled Stylesheet ===*/

	wp_register_style('custom_styles', get_stylesheet_directory_uri() . '/css/style.min.css');
	wp_enqueue_style('custom_styles');

}

add_action('wp_enqueue_scripts', 'wp_load_files', 11);

/*===================================
Typekit
===================================*/

function theme_typekit() {
    wp_enqueue_script( 'theme_typekit', '//use.typekit.net/qfs0max.js');
}
add_action( 'wp_enqueue_scripts', 'theme_typekit' );
function theme_typekit_inline() {
  if ( wp_script_is( 'theme_typekit', 'done' ) ) { ?>
  	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<?php }
}
add_action( 'wp_head', 'theme_typekit_inline' );

/*===================================
Overwrite Bridge Includes
===================================*/

// Overwrite custom post types created by Bridge theme

require_once( get_stylesheet_directory() . '/includes/qode-custom-post-types.php' );

/*=============================================
CUSTOM LOGIN SCREEN
=============================================*/

// Change the login logo URL

function my_loginURL() {
    return 'http://everybreathcounts.info';
}
add_filter('login_headerurl', 'my_loginURL');

// Enque the login specific stylesheet for design customizations. CSS file is compiled through compass.

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_stylesheet_directory_uri() . '/css/login.min.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

/*=============================================
YOAST
=============================================*/

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

/*=============================================
DEBUGGING
=============================================*/
function r($var){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}
function d($var){
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}
